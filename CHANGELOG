[0.1.0]
* Initial version

[1.0.0]
* Add octave support

[1.0.1]
* Update JupyterHub to 0.9.4

[1.1.0]
* Make config.py customizable
* Put lot more packages into the notebook image

[1.2.0]
* Make notebook image customizable

[1.3.0]
* Use latest base image

[1.4.0]
* Update JupyterHub to 0.9.6

[1.4.1]
* Update dockerspawner to 0.11.1

[1.5.0]
* Update JupyterHub to 1.0.0

[1.6.0]
* Use manifest v2
* Update docker-py to 3.7.3

[1.7.0]
* Use datascience-notebook:2ce7c06a61a1
* Use Jupyter Lab by default

[1.8.0]
* Use datascience-notehook:1386e2046833

[1.9.0]
* Use datascience-notebook to 45f07a14b422

[1.10.0]
* Use datascience-notebook to 7a0c7325e470

[1.11.0]
* Use datascience-notebook 678ada768ab1

[1.12.0]
* Use datascience-notebook dc9744740e12

[1.13.0]
* Use datascience-notebook 619e9cc2fc07
* Use base image 2.0.0

[1.14.0]
* Use datascience-notbook 3b1f4f5e6cc1

[1.15.0]
* Use databascience-notebook 5197709e9f23
* Update manifest screenshots and forum url

[1.16.0]
* Use datascience-notebook 1a66dd36ff82

[1.17.0]
* Update datascience-notebook to dc62000e452f
* Update LDAP authenticator to 1.3.2

[1.18.0]
* Update datascience notebook to 95ccda3619d0
* Update JupyterHub to 1.2.1

[1.18.1]
* Update datascience notebook to 5cfa60996e84
* Update JupyterHub to 1.2.2
* [Full changelog](https://github.com/jupyterhub/jupyterhub/releases/tag/1.2.2)

[1.19.0]
* Update JupyterHub to 1.3.0
* [Full changelog](https://github.com/jupyterhub/jupyterhub/releases/tag/1.3.0)

[1.20.0]
* Update datascience notebook to 703d8b2dcb88

[1.21.0]
* Use base image v3
* Update datascience notebook to 016833b15ceb

[1.22.0]
* Update dockerspawner to 12.0.0
* Update datascience notebook to 3395de4db93a

[1.23.0]
* Update JupyterHub to 1.4.0
* Update datascience notebook to 584f43f06586

[1.23.1]
* Update JupyterHub to 1.4.1
* Update datascience notebook to cf9a75c29789

[1.24.0]
* Update JupyterHub to 1.4.2
* Update datascience notebook to ecabcb829fe0

[1.25.0]
* Update datascience notebook to b9f6ce795cfc

[1.26.0]
* Update datascience notebook to 061a02b21fea

[1.27.0]
* Update JupyterHub to 1.5.0

[1.28.0]
* Update JupyterHub to 2.1.0
* Update data science notebook to 2fee7b754cbe
* Update base image to 3.2.0

[1.29.0]
* Update JupyterHub to 2.2.0
* Update data science notebook to fd4f082df81c

[1.30.0]
* Update JupyterHub to 2.2.1

[1.31.0]
* Update JupyterHub to 2.2.2
* Update datascience-notebook to f6ac8f26bd30
* Update miniconda to 4.11.0

[1.32.0]
* Update JupyterHub to 2.3.0
* Update datascience-notebook to notebook-6.4.11

[1.32.1]
* Update JupyterHub to 2.3.1

[1.32.2]
* Update datascience-notebook to notebook-6.4.12

[1.33.0]
* Update JupyterHub to 3.0.0

[1.34.0]
* Update base image to 4.0.0

[1.35.0]
* Update JupyterHub to 3.1.0

[1.36.0]
* Update JupyterHub to 3.1.1
* Update datascience-notebook to notebook-6.5.2

[1.36.1]
* Update datascience-notebook to notebook-6.5.3

[1.37.0]
* Update JupyterHub to 4.0.0
* Update datascience-notebook to notebook-6.5.4

[1.38.0]
* Update JupyterHub to 4.0.1
* Update datascience-notebook to lab-4.0.3

[1.38.1]
* Update datascience-notebook to lab-4.0.4

[1.38.2]
* Update JupyterHub to 4.0.2

[1.38.3]
* Update datascience-notebookt to lab-4.0.5

[1.38.4]
* Update datascience-notebookt to lab-4.0.6

[1.38.5]
* Update datascience-notebookt to lab-4.0.7

[1.39.0]
* Update docker spawner to 13.0.0. Fixes security issue GHSA-hfgr-h3vc-p6c2

[1.40.0]
* Fix container removal script
* Allow users to launch multiple environments

[1.41.0]
* Update docker-py to 7.0.0

[1.41.1]
* Fix notebook container name to use app hostname instead of domain

[1.42.0]
* Use OIDC for authentication

[1.43.0]
* IMPORTANT CHANGE: Starting this release, notebook containers are never deleted by the package because:
  * This prevents long running notebooks for getting deleted
  * Upstream has a way to let user's choose which image they want (`c.DockerSpawner.allowed_images`). This means
    that it is not correct to assume user always wants the latest and greatest notebook image.

[1.44.0]
* Update datascience notebook image to lab-4.1.5
* Update miniconda3 to py38_23.11.0-2
* Update docker to 25.0.5

[1.45.0]
* Update JupyterHub to 4.1.3

[1.45.1]
* Update JupyterHub to 4.1.4

[1.45.2]
* Update JupyterHub to 4.1.5

[1.45.3]
* Update datascience notebook image to lab-4.1.6

[1.45.4]
* Update datascience notebook image to lab-4.1.8

[1.45.5]
* Update datascience notebook image to lab-4.2.2

[1.46.0]
* Update JupyterHub to 5.0.0

[1.47.0]
* Update JupyterHub to 5.1.0
* Update datascience notebook image to lab-4.2.4

[1.48.0]
* Update docker pypi to 7.1.0
* Bumped minimum engine API version to 1.24
* Bumped default engine API version to 1.44 (Moby 25.0)

[1.49.0]
* Update oauthenticator to 17.0.0

[1.50.0]
* Update jupyterhub to 5.2.1

[1.50.1]
* Update datascience-notebook to 4.2.5

[1.50.2]
* Update datascience-notebook to 4.2.6

[1.51.0]
* Update oauthenticator to 17.2.0
* [Full Changelog](https://github.com/jupyterhub/jupyterhub/releases/tag/1.3.0)

[1.52.0]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.52.1]
* Update datascience-notebook to 4.3.2

[1.53.0]
* Update oauthenticator to 17.3.0

[1.53.1]
* Update datascience-notebook to 4.3.3

[1.53.2]
* Update datascience-notebook to 4.3.4

[1.53.3]
* Update datascience-notebook to 4.3.5
* [Full Changelog](https://github.com/jupyterhub/jupyterhub/releases/tag/1.3.0)

[1.54.0]
* Update dockerspawner to 14.0.0
* Update python to 3.12

