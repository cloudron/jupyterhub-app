
## History

The code idea of Jupyter is to have programmable notebooks. The architecture is a
client/server model where the client is a notebook (web ui) or QtNotebook (Qt!).
The server basically spawns off "kernels" which provide language support. On the
server side, the server and the kernel talk using ZeroMQ. This ZeroMQ can be then
used to keep the server and kernels in some distributed architecture.

It all started with IPython which provide a simple python notebook. Later this project
became more and more language agnostic and it got renamed to Jupyter.

So have different kinds of "IPython kernels" which basically means kernels that support
this IPython protocol that lets this notebook server talk to the kernel via ZeroMQ.

The IPython kernel is the Python execution backend for Jupyter. The IPython project provides
support for Python2/3 using the ipykernel module.

## Notebook / JupyterLab

JupyterLab has a extensible modular architecture than the legacy "notebook". So in the old days,
there is just one Jupyter Notebook, and now with JupyterLab (and in the future), Notebook is
just one of the core applications in JupyterLab (along with others like code Console, command-line Terminal,
and a Text Editor).

Notebook v7 will be based off "lab" stuff - https://github.com/jupyter/notebook#notebook-v7

## JupyterHub

This notebook server is really a single user notebook server. They added JupyterHub
which lets a user login and it spins off a notebook server for each user. They
can be spun off as docker containers which is what we use.

## Kernels

To install the kernel, it is just `pip install ipython`. You can then make the kernel
available by using. `ipython install`. This is just copying a kernel specification file
that jupyter can find. One can alternately do `ipython install --prefix /tmp`, then edit
the kernel json files and then install it using `jupyter kernelspec install /tmp/share/jupyter/kernels/python3`.

Installed kernels - `jupyter kernelspec list` . Note that this has to done in the
notebook container that jupyterhub spawns and not in this app's Dockerfile!

The kernels have specs associated with them which tell the notebook server how to
launch it. See, `/opt/conda/share/jupyter/kernels/*/kernel.json` in the notebook
container image.

https://ipython.readthedocs.io/en/stable/install/kernel_install.html
https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernelspecs

## Various modules

* notebook - this is the notebook server that is spun off by the hub
* jupyterhub - this provides authentication
* jupyterhub-ldapauthenticator - brings ldap auth to jupyterhub
* dockerspawner - once auth'ed, jupyterhub will create notebook servers by using this spawner
    * the `image` (jupyter/datascience-notebook) in config must have the same
      jupyterhub version as we install above. This is required for the communication protocol
      to match. https://github.com/jupyter/docker-stacks/blob/master/base-notebook/Dockerfile#L102
* proxy - aka configurable http proxy
    * users connect to the proxy and the proxy will redirect to the hub for auth or to the notebook
    server based on session/cookie. this is built-in to the hub but can be run as a sepearate entity
    allowing the hub to be updated/restarted without the user losing connectivity to their notebooks.

## Culling

Starting notebook 5.1, notebook has a way to ping kernels for activity.

https://jupyter-notebook.readthedocs.io/en/stable/config.html?highlight=cull
https://jupyterhub.readthedocs.io/en/stable/reference/config-user-env.html#example-enable-a-jupyter-notebook-configuration-setting-for-all-users
https://github.com/jupyter/notebook/pull/2215
https://jupyterhub.readthedocs.io/en/stable/getting-started/spawners-basics.html

The kernel is shutdown after inactivity. And then notebook also shutdown if
there is attached kernel.

## New image

We use the image in `notebook-container` for notebooks:

```
cd notebook-container
docker build -t cloudron/jupyter-app-notebook .
docker push cloudron/jupyter-app-notebook
```

After that update `start.sh` with the correct sha256

