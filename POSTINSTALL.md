This app is configured to use the [data science](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-datascience-notebook)
notebook. See the docs on how to set an [alternate notebook](https://cloudron.io/documentation/apps/jupyterhub/#selecting-a-notebook-image).

