# Was derived from jupyterhub --generate-config
# https://github.com/jupyterhub/jupyterhub-deploy-docker#run-jupyterhub has a diagram

import logging
import os

c.Application.log_level = logging.DEBUG

c.JupyterHub.active_server_limit = 0
c.JupyterHub.admin_access = False

# proxy bind url
c.JupyterHub.bind_url = 'http://:8000'  # configurable http proxy (https://github.com/jupyterhub/jupyterhub/blob/main/docs/source/reference/separate-proxy.md)
c.JupyterHub.cleanup_proxy = True
c.JupyterHub.cleanup_servers = True
c.JupyterHub.concurrent_spawn_limit = 100

# private url on which the hub will listen. notebook server containers run outside 'cloudron' network, so we have to also listen on IPv6 to as we set the hub_connect_ip to a hostname. alternative, is to set hub_connect_ip to the ipv4 but this relies on the fact that we hardcode app IPs on Cloudron.
c.JupyterHub.hub_bind_url = 'http://:8081' 
# how do spawners and proxy connect to hub. starting 3.3.0, this HOSTNAME is 'constant'. when it was variable,
# the notebook containers are unable to connect after an update which changes the container hostname (https://github.com/jupyterhub/dockerspawner/issues/204)
c.JupyterHub.hub_connect_ip = os.environ['HOSTNAME']

c.JupyterHub.cookie_secret_file = '/app/data/jupyterhub_cookie_secret' # generated in start.sh

c.JupyterHub.data_files_path = '/usr/local/conda/share/jupyterhub'

c.JupyterHub.db_url = 'sqlite:///jupyterhub.sqlite'

def create_dir_hook(spawner):
    username = spawner.user.name # get the username
    volume_path = os.path.join('/app/data/notebooks', username)
    print('Creating user directory %s' % volume_path)
    if not os.path.exists(volume_path):
        os.mkdir(volume_path, 0o755)
    os.chown(volume_path, 1000, 100)

c.Spawner.pre_spawn_hook = create_dir_hook
# c.Spawner.post_stop_hook = clean_dir_hook
c.Spawner.default_url = '/lab'

# this will allow a user to create multiple environments with a servername
# when set, the default container names are now {prefix}-{username}{servername}
c.JupyterHub.allow_named_servers = True

c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.use_internal_ip = True
# allocates equal amount of swap
c.Spawner.mem_limit = '500M'
# set container name prefix, so that we can delete containers in remove_notebook_containers.py
c.DockerSpawner.prefix = os.environ['CLOUDRON_APP_HOSTNAME']
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = 'cloudron'
c.DockerSpawner.extra_host_config = { 'network_mode': 'cloudron' }
c.DockerSpawner.image = os.environ['NOTEBOOK_IMAGE']
c.DockerSpawner.prefix = os.environ['CLOUDRON_APP_HOSTNAME']
c.DockerSpawner.notebook_dir = '/home/jovyan' # this comes from the docker image
c.DockerSpawner.volumes = { '/app/data/notebooks/{username}': '/home/jovyan' }
# this removes idle/stopped containers resulting on loss of non-volume data. we clean up all containers on image change now
c.DockerSpawner.remove_containers = False
c.DockerSpawner.debug = True
# specify that DockerSpawner should accept any image from user input
c.DockerSpawner.allowed_images = "*"
# give up to 5 mins for image to get pulled
c.DockerSpawner.start_timeout = 300

# OIDC
c.JupyterHub.authenticator_class = "generic-oauth"
c.OAuthenticator.oauth_callback_url = os.environ['CLOUDRON_APP_ORIGIN'] + '/hub/oauth_callback'
c.GenericOAuthenticator.client_id = os.environ['CLOUDRON_OIDC_CLIENT_ID']
c.GenericOAuthenticator.client_secret = os.environ['CLOUDRON_OIDC_CLIENT_SECRET']
c.GenericOAuthenticator.login_service = os.environ['CLOUDRON_OIDC_PROVIDER_NAME']

c.GenericOAuthenticator.authorize_url = os.environ['CLOUDRON_OIDC_AUTH_ENDPOINT']
c.GenericOAuthenticator.token_url = os.environ['CLOUDRON_OIDC_TOKEN_ENDPOINT']
c.GenericOAuthenticator.userdata_url = os.environ['CLOUDRON_OIDC_PROFILE_ENDPOINT']
c.GenericOAuthenticator.username_claim = "sub"
c.GenericOAuthenticator.scope = ["openid", "profile", "email"]
c.OAuthenticator.allow_existing_users = True
c.OAuthenticator.allow_all = True

