#!/bin/bash

set -eu

# required for docker-py and dockerspawner
export DOCKER_HOST="${CLOUDRON_DOCKER_HOST}"

[[ -z "${CLOUDRON_OIDC_PROVIDER_NAME:-}" ]] && export CLOUDRON_OIDC_PROVIDER_NAME="Cloudron"

mkdir -p /app/data/notebooks /run/jupyterhub

if [[ ! -f /app/data/jupyterhub_cookie_secret ]]; then
    echo "==> Generating cookie secret"
    openssl rand -hex 32 > /app/data/jupyterhub_cookie_secret
fi
chmod 0500 /app/data/jupyterhub_cookie_secret

[[ ! -f /app/data/customconfig.py ]] && cp /app/code/customconfig.py.template /app/data/customconfig.py

cat /app/code/config.py /app/data/customconfig.py > /run/jupyterhub/config.py

# This is the user 'joyvan' and group 'users' in the child notebook container
# https://github.com/jupyter/docker-stacks/blob/master/base-notebook/Dockerfile
echo "==> Changing ownership of notebooks"
chown -R 1000:100 /app/data/notebooks

echo "==> Starting JupyterHub"
cd /app/data

# currently runs as root for the create_dir_hook to chown
exec jupyterhub --upgrade-db --config /run/jupyterhub/config.py

