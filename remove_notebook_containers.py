#!/usr/bin/env python

import docker
import os

os.environ['DOCKER_HOST'] = os.environ['CLOUDRON_DOCKER_HOST']

client = docker.from_env()

remove_count = 0

for notebook in client.containers.list(all = True):
    if notebook.name.startswith(os.environ['CLOUDRON_APP_HOSTNAME'] + '-'):
        print('Removing container with id %r and name %r' % (notebook.id, notebook.name) )
        notebook.remove(force = True)
        remove_count = remove_count + 1

print('Removed %r containers' % (remove_count))

