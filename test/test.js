#!/usr/bin/env node

/* jshint esversion: 6 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const LOCATION = process.env.LOCATION || 'test';

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/hub/login`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath(`//a[contains(., "Sign in with ${cloudronName}")]`)), TIMEOUT);
        await browser.findElement(By.xpath(`//a[contains(., "Sign in with ${cloudronName}")]`)).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(40000); // wait generously for notebook container to get created
        await waitForElement(By.xpath('//div[contains(text(), "Kernel")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/logout');
        await browser.sleep(3000);
    }

    async function pushNotebook() {
        execSync(`cloudron push --app ${app.fqdn} ${__dirname}/graph.ipynb /app/data/notebooks/${username}/graph.ipynb`, EXEC_ARGS);
        execSync(`cloudron exec --app ${app.fqdn} chown cloudron:users /app/data/notebooks/${username}/graph.ipynb`, EXEC_ARGS);

        execSync(`cloudron push --app ${app.fqdn} ${__dirname}/octave_kernel.ipynb /app/data/notebooks/${username}/octave_kernel.ipynb`, EXEC_ARGS);
        execSync(`cloudron exec --app ${app.fqdn} chown cloudron:users /app/data/notebooks/${username}/octave_kernel.ipynb`, EXEC_ARGS);
    }

    async function verifyGraphNotebook() {
        await browser.get(`https://${app.fqdn}/user/${username}/notebooks/graph.ipynb`);
        await browser.wait(until.elementLocated(By.xpath('//*[@class="MathJax" and @jax="CHTML"]')), TIMEOUT);
    }

    // function verifyOctaveNotebook() {
    //     return browser.get('https://' + app.fqdn + '/user/girish/notebooks/octave_kernel.ipynb').then(function () {
    //         return browser.wait(until.elementLocated(By.xpath('//i[@title="Kernel Idle"]')), TIMEOUT);
    //     });
    // }

    async function verifyNotebook() {
        await verifyGraphNotebook();
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password, false));
    it('can push notebook', pushNotebook);
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.jupyter.cloudronapp --location ' + LOCATION, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can push notebook', pushNotebook);
    it('verify notebook', verifyNotebook);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('verify notebook', verifyNotebook);
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
