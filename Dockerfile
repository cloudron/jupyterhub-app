FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# select image from https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html
# https://quay.io/repository/jupyter/datascience-notebook?tab=tags
# renovate: datasource=docker depName=quay.io/jupyter/datascience-notebook versioning=semver extractVersion=^lab-(?<version>.*)$
ARG NOTEBOOK_VERSION=4.3.5
ENV NOTEBOOK_IMAGE=quay.io/jupyter/datascience-notebook:lab-${NOTEBOOK_VERSION}

RUN mkdir -p /app/code
WORKDIR /app/code

# miniconda on python3
ENV PATH /usr/local/conda/bin:$PATH

# https://repo.continuum.io/miniconda/
RUN curl -L https://repo.continuum.io/miniconda/Miniconda3-py312_25.1.1-2-Linux-x86_64.sh -o /tmp/miniconda.sh  && \
    bash /tmp/miniconda.sh -f -b -p /usr/local/conda && \
    conda install --yes --solver classic -c conda-forge sqlalchemy tornado jinja2 traitlets requests pip pycurl configurable-http-proxy && \
    pip install --upgrade pip && \
    rm /tmp/miniconda.sh

# https://pypi.org/project/dockerspawner/#history
# renovate: datasource=pypi depName=dockerspawner versioning=pep440
ARG DOCKERSPAWNER_VERSION=14.0.0

# https://pypi.org/project/docker/#history
# renovate: datasource=pypi depName=docker versioning=pep440
ARG DOCKER_VERSION=7.1.0
RUN pip install dockerspawner==${DOCKERSPAWNER_VERSION} docker==${DOCKER_VERSION}

# https://anaconda.org/conda-forge/jupyterhub
# renovate: datasource=conda depName=conda-forge/jupyterhub versioning=semver
ARG JUPYTERHUB_VERSION=5.2.1

 # renovate: datasource=conda depName=conda-forge/oauthenticator versioning=semver
ARG OAUTHENTICATOR_VERSION=17.3.0
RUN conda install --yes -c conda-forge jupyterhub=${JUPYTERHUB_VERSION} oauthenticator=${OAUTHENTICATOR_VERSION} && \
    conda update --yes -c conda-forge --all

# https://download.docker.com/linux/static/stable/x86_64/
RUN cd /usr/bin && curl -L https://download.docker.com/linux/static/stable/x86_64/docker-25.0.5.tgz | tar -zxvf - --strip-components=1 docker/docker

RUN mkdir -p /app/code
COPY config.py /app/code/config.py
COPY customconfig.py.template /app/code/customconfig.py.template
COPY remove_notebook_containers.py /app/code/remove_notebook_containers.py
COPY start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
